import json

from azure.storage.blob import BlobServiceClient


def load_extract_load_status(
    status_responses: str,
    conn_string: str,
    container: str,
    blob_path: str,
    blob_name: str) -> None:
    """Upload extrac/load statuses to blob storage in json format
    
    Parameters
    ----------
    status_responses: list
        List of status responses retrieved by retrieve_data activity.
    conn_string: str
        Connection string to connect to blob storage.
    container: str
        Container where status responses json will be stored.
    blob_path: str
        Path of the json file.
    blob_name: str
        Name of the json file.
    """

    check_data_content = json.dumps(status_responses, indent=4)

    blob_service_client = BlobServiceClient.from_connection_string(conn_string)
    blob_client = blob_service_client.get_blob_client(
        container=container,
        blob=blob_path + blob_name
    )

    blob_client.upload_blob(data=check_data_content, overwrite=True)
