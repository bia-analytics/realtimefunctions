from configparser import (
    ConfigParser,
    ExtendedInterpolation
)
from dataclasses import dataclass
from pathlib import Path

from .extract import (
    ExtractBlob,
    extract_data,
    get_engine
)
from .load import load_to_blob_storage
from .status import load_extract_load_status
from .transform import SalesTableComposer


def get_tables_to_query():
    config_path = Path(__file__).parents[1] / 'config_tables.ini'
    
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(config_path)

    return config