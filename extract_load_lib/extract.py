"""Library for extracting data from various data sources."""

from datetime import datetime, timedelta
import io
import logging
import urllib

from azure.storage.blob import BlobClient
from azure.storage.blob import BlobServiceClient
import pandas as pd
from pandas.core.frame import DataFrame
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine


def get_engine(
    host: str, 
    port: str, 
    dbname: str, 
    user: str, 
    password: str,
    timeout: int) -> Engine:
    """Creates an sqlalchemy engine for the provided db settings.
    
    Parameters
    ----------
    host: str
        Database host (e.g. 10.23.50.10).
    port: str
        Database port (e.g. usually 1433 for MSSQL).
    dbname: str
        Database name.
    user: str
        User for login database.
    password: str
        Password for logging into database (it doesn't need to be HTML 
        encrypted).

    Returns
    -------
    Engine
        Engine SQLAlchemy object.
    """
    
    logging.debug('engine creation start')
    
    password = urllib.parse.quote_plus(password)
    connect_args = {'timeout': int(timeout)}
    conn_string = 'mssql+pyodbc://' \
        f'{user}:' \
        f'{password}@' \
        f'{host}:{port}' \
        f'/{dbname}?' \
        'driver=ODBC+Driver+17+for+SQL+Server'
    
    logging.debug(f'conn string: {conn_string}')

    engine = create_engine(conn_string, connect_args=connect_args)

    logging.debug('engine creation end')

    return engine

def extract_data(engine: Engine, query_string: str) -> DataFrame:
    """Retrieve database data as a pandas dataframe.
    
    Parameters
    ----------
    query_string: str
        Straight query string (e.g. select * from table).
    
    Returns
    -------
    DataFrame
        Pandas DataFrame object.
    """
    
    logging.debug('extract process start')

    df = pd.read_sql(query_string, engine)

    logging.debug(f'dataframe sample: \n {df}')
    logging.debug(f'dataframe datatypes: \n {df.dtypes}')
    logging.debug('extract process end')

    return df

class ExtractBlob:
    """Class for extracting data from blob storage"""

    def __init__(self, conn_string):
        self.conn_string = conn_string
        self.blob_service_client = BlobServiceClient \
            .from_connection_string(self.conn_string)

    def read_blob(
        self,
        input_format: str,
        container: str,
        blob_path: str,
        blob_name: str,
        output_format: str = 'dataframe',
    ) -> DataFrame:
        """Read blob from storage"""

        blob_client = self.blob_service_client.get_blob_client(
            container=container,
            blob=blob_path + blob_name
        )
        stream_downloader = blob_client.download_blob()
        stream = io.BytesIO()
        stream_downloader.readinto(stream)

        read_func = {
            'csv': self._read_csv,
            'parquet': self._read_parquet
        }

        input = read_func[input_format](stream) 

        output_func = {
            'dict': self._output_dict
        }

        if output_format == 'dataframe':
            return input
        else:
            return output_func[output_format](input)
    
    @staticmethod
    def _output_dict(dataframe: DataFrame) -> dict:
        """Return an dict output from a dataframe"""

        return dataframe.to_dict(orient='records')

    @staticmethod
    def _read_csv(stream: io.BytesIO) -> DataFrame:
        """Read csv from stream"""

        return pd.read_csv(io.StringIO(stream.getvalue().decode()), sep=';')
    
    @staticmethod
    def _read_parquet(stream: io.BytesIO) -> DataFrame:
        """Read parquet from stream"""

        return pd.read_parquet(stream, engine='pyarrow')
    
    def try_recursive_read(
        self,
        days_to_try: int,
        format: str,
        container: str,
        prefix_path: str,
        prefix_name: str
    ) -> DataFrame:
        
        init_date = datetime.today().date() - timedelta(days=days_to_try - 1)
        date_range = pd.date_range(init_date, periods=days_to_try).to_list()
        date_range.reverse()
        
        for date in date_range:
            blob_path = f'{prefix_path}/{date.year}/{date.month:02}/{date.day:02}/'
            blob_name = f'{prefix_name}_{date.strftime("%Y%m%d")}.{format}'
            try:
                return self.read_blob(format, container, blob_path, blob_name)
            except:
                logging.info(f'could not find {blob_name}')
        
        raise Exception(f'Could not find any {prefix_name} file recursively')