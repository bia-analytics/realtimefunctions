import azure.durable_functions as df

from env_vars_lib import get_env_vars


def orchestrator_function(context: df.DurableOrchestrationContext):

    env_vars = get_env_vars()
    filedata = {
        'conn_string': env_vars.storage_conn_string,
        'container': env_vars.source_container,
        'database_list_file_path': env_vars.database_list_file_path,
        'database_list_file_name': env_vars.database_list_file_name
    }
    db_list = yield context.call_activity('get_databases', filedata)

    tasks = []
    for datadase_info in db_list:
        tasks.append(context.call_activity('retrieve_data', datadase_info))
    
    results = yield context.task_all(tasks)
    
    yield context.call_activity('upload_status', results)

main = df.Orchestrator.create(orchestrator_function)