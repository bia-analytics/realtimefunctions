from datetime import datetime

import azure.functions as func
from azure.data.tables import TableClient
import pandas as pd

from env_vars_lib import get_env_vars
from extract_load_lib import load_to_blob_storage


def main(mytimer: func.TimerRequest) -> None:
    
    env_vars = get_env_vars()

    table_client = TableClient.from_connection_string(
        env_vars.storage_conn_string, 
        env_vars.realtimesales_storage_table
    )
    data = list(table_client.list_entities())

    df = pd.DataFrame(data)

    blob_name = f'real_time_sales_{datetime.today().isoformat()}.csv'
    load_to_blob_storage(
        df,
        'csv',
        env_vars.storage_conn_string,
        env_vars.stream_container,
        env_vars.stream_folder,
        blob_name
    )