from datetime import datetime

from env_vars_lib import get_env_vars
from extract_load_lib import load_extract_load_status


def main(statusresponses: list) -> None:

    env_vars = get_env_vars()

    today = datetime.today()

    conn_string = env_vars.storage_conn_string
    container = env_vars.source_container
    blob_path = env_vars.status_log_path \
        + f'{today.year:04}/{today.month:02}/{today.day:02}/'
    blob_name = f'status_{today.strftime("%Y%m%d_%H%M%S")}.json'

    try:
        load_extract_load_status(
            status_responses=statusresponses,
            conn_string=conn_string,
            container=container,
            blob_path=blob_path,
            blob_name=blob_name
        )
        return 'status successfully uploaded.'
    except Exception:
        raise