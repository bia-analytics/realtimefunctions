import os

import pytest


@pytest.fixture(scope='module')
def storage_conn_string():
    return os.environ.get('STORAGE_CONN_STRING')

@pytest.fixture(scope='module')
def bronze_container_name():
    return os.environ.get('SOURCE_CONTAINER')

@pytest.fixture(scope='module')
def gold_container_name():
    return os.environ.get('STREAM_CONTAINER')

@pytest.fixture(scope='module')
def db_host():
    return os.environ.get('DB_HOST')

@pytest.fixture(scope='module')
def db_port():
    return os.environ.get('DB_PORT')

@pytest.fixture(scope='module')
def db_name():
    return os.environ.get('DB_NAME')

@pytest.fixture(scope='module')
def db_login():
    return os.environ.get('DB_LOGIN')

@pytest.fixture(scope='module')
def db_password():
    return os.environ.get('DB_PASSWORD')