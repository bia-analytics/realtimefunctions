import io
import json
import logging

from azure.storage.blob import BlobServiceClient
import pytest

from extract_load_lib import load_extract_load_status


@pytest.fixture(scope='function')
def status_blob_path():
    return 'test/status/'

@pytest.fixture(scope='function')
def status_blob_name():
    return 'test.json'

@pytest.fixture
def status_response():
    return {
        "store": None,
        "status": "something got wrong",
        "trace": "division by zero"
    }

def test_load_extract_load_status(
    storage_conn_string, 
    bronze_container_name, 
    status_blob_path,
    status_blob_name,
    status_response):

    load_extract_load_status(
        status_responses=status_response,
        conn_string=storage_conn_string,
        container=bronze_container_name,
        blob_path=status_blob_path,
        blob_name=status_blob_name
    )

    blob_client = BlobServiceClient.from_connection_string(storage_conn_string) \
        .get_blob_client(
            bronze_container_name, 
            f'{status_blob_path}{status_blob_name}')
        
    stream_downloader = blob_client.download_blob()
    stream = io.BytesIO()
    stream_downloader.readinto(stream)

    content = json.loads(stream.getvalue().decode())

    logging.debug(content)

    assert content

    blob_client.delete_blob()