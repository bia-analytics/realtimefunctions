import logging
import os

import pytest

from extract_load_lib import (
    ExtractBlob,
    extract_data,
    get_engine
)


@pytest.fixture
def query_string():
    return os.environ.get('DB_TEST_QUERY_STRING')

@pytest.fixture
def engine(db_host, db_port, db_name, db_login, db_password):
    engine = get_engine(
        host=db_host,
        port=db_port,
        dbname=db_name,
        user=db_login,
        password=db_password,
        timeout=5
    )
    yield engine
    engine.dispose()

@pytest.fixture
def extract_blob_prefix_path():
    return os.environ.get('EXTRACT_BLOB_PREFIX_PATH')

@pytest.fixture
def extract_blob_prefix_name():
    return os.environ.get('EXTRACT_BLOB_PREFIX_NAME')

@pytest.fixture
def extract_csv_blob_path():
    return os.environ.get('EXTRACT_CSV_BLOB_PATH')

@pytest.fixture
def extract_csv_blob_name():
    return os.environ.get('EXTRACT_CSV_BLOB_NAME')

def test_engine_connection(engine):
    conn = engine.connect()
    assert conn
    conn.close()

def test_extract_data_from_database(engine, query_string):
    df = extract_data(engine, query_string)
    assert df.empty == False

def test_existing_extract_blob(
    storage_conn_string, 
    bronze_container_name, 
    extract_blob_prefix_path, 
    extract_blob_prefix_name):
    
    df = ExtractBlob(storage_conn_string).try_recursive_read(
        days_to_try=3,
        container=bronze_container_name,
        prefix_path=extract_blob_prefix_path,
        prefix_name=extract_blob_prefix_name
    )

    assert df.empty == False

def test_nonexisting_extract_blob(
    storage_conn_string,
    bronze_container_name):

    with pytest.raises(Exception):
        df = ExtractBlob(storage_conn_string).try_recursive_read(
            days_to_try=3,
            container=bronze_container_name,
            prefix_path='test',
            prefix_name='test'
        )

def test_read_csv_blob(
    storage_conn_string, 
    bronze_container_name,
    extract_csv_blob_path,
    extract_csv_blob_name
):
    df = ExtractBlob(storage_conn_string).read_blob(
        'csv',
        bronze_container_name,
        extract_csv_blob_path,
        extract_csv_blob_name
    )

    # df['VALOR'] = df['VALOR'].str.replace(',', '.').astype(float)
    # df['DATA'] = pd.to_datetime(df['DATA'])

    logging.debug(df)
    logging.debug(df.dtypes)

    assert df.empty == False