from dataclasses import dataclass
import os


ENV_VARS = None

@dataclass
class EnvVars:
    """Class for storing environment variables."""

    storage_conn_string: str
    stream_container: str
    stream_folder: str
    source_container: str
    loja_vendedores_source_prefix_path: str
    filiais_source_prefix_path: str
    sql_timeout: int
    registry_tables_recursive_tries: int
    realtimesales_storage_table: str
    yesterdaey_realtimesales_storage_table: str
    goals_blob_path: str
    goals_blob_name: str
    status_log_path: str
    database_list_file_path: str
    database_list_file_name: str

def get_env_vars():
    """Read environment variables and store in global var ENV_VAR."""

    global ENV_VARS
    if not ENV_VARS:
        ENV_VARS = EnvVars(
            storage_conn_string=os.environ.get('AzureWebJobsSTORAGE_CONN_STRING'),
            stream_container=os.environ.get('STREAM_CONTAINER'),
            stream_folder=os.environ.get('STREAM_FOLDER'),
            source_container=os.environ.get('SOURCE_CONTAINER'),
            loja_vendedores_source_prefix_path=
                os.environ.get('LOJA_VENDEDORES_SOURCE_PREFIX_PATH'),
            filiais_source_prefix_path=
                os.environ.get('FILIAIS_SOURCE_PREFIX_PATH'),
            sql_timeout=os.environ.get('SQL_TIMEOUT'),
            registry_tables_recursive_tries=
                os.environ.get('REGISTRY_TABLES_RECURSIVE_TRIES'),
            realtimesales_storage_table=
                os.environ.get('REALTIMESALES_STORAGE_TABLE'),
            yesterdaey_realtimesales_storage_table=
                os.environ.get('YESTERDAY_REALTIMESALES_STORAGE_TABLE'),
            goals_blob_path=os.environ.get('GOALS_BLOB_PATH'),
            goals_blob_name=os.environ.get('GOALS_BLOB_NAME'),
            status_log_path=os.environ.get('STATUS_LOG_PATH'),
            database_list_file_path=
                os.environ.get('DATABASE_LIST_FILE_PATH'),
            database_list_file_name=
                os.environ.get('DATABASE_LIST_FILE_NAME')
        )
    
    return ENV_VARS