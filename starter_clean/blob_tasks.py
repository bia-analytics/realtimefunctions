from azure.storage.blob import ContainerClient


def move_blob(
    container_client: ContainerClient, 
    source_path: str, 
    target_path: str
):
    """Move a blob from source to target in a same container"""

    source_blob_client = container_client.get_blob_client(blob=source_path)
    target_blob_client = container_client.get_blob_client(blob=target_path)

    target_blob_client.start_copy_from_url(source_blob_client.url)
    copy_properties = target_blob_client.get_blob_properties().copy

    if copy_properties.status != 'success':
        target_blob_client.abort_copy(copy_properties.id)
        raise Exception(f'Unable to copy blob {source_path} ' \
                f'with status {copy_properties.status}'
        )
    
    source_blob_client.delete_blob()

    return copy_properties.status