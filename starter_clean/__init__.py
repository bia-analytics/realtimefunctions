import itertools
import time

import azure.functions as func
from azure.data.tables import TableServiceClient
from azure.storage.blob import BlobServiceClient

from env_vars_lib import get_env_vars
from .blob_tasks import move_blob


def main(mytimer: func.TimerRequest) -> None:

    env_vars = get_env_vars()

    table_service_client = TableServiceClient.from_connection_string(
        env_vars.storage_conn_string)
    
    today_table = table_service_client.get_table_client(
        env_vars.realtimesales_storage_table)
    table_service_client.delete_table(
        env_vars.yesterdaey_realtimesales_storage_table)
    time.sleep(60)
    yesterday_table = table_service_client.create_table(
        env_vars.yesterdaey_realtimesales_storage_table)

    today_records = list(today_table.list_entities())
    record_groups = [list(record) for _, record in itertools.groupby(today_records, lambda x: x['PartitionKey'])]
    for group in record_groups:
        record_insert = [('create', record) for record in group]
        record_delete = [('delete', record) for record in group]

        yesterday_table.submit_transaction(record_insert)
        today_table.submit_transaction(record_delete)
        
    blob_service_client = BlobServiceClient.from_connection_string(
        env_vars.storage_conn_string)
    
    container_client = blob_service_client.get_container_client(
        env_vars.stream_container)
    blob_list = list(container_client.list_blobs(
        name_starts_with='realtime/sales/merged/'))
    csv_blob_list = [blob for blob in blob_list if \
        blob['name'].endswith('.csv')]
    for csv_blob in csv_blob_list:
        container_client.get_blob_client(csv_blob['name']).delete_blob()


    container_client = blob_service_client.get_container_client(
        env_vars.source_container)
    blob_list = list(container_client.list_blobs(
        name_starts_with='linx_pos/RealTime/'))
    yesterday_parquet_list = [blob for blob in blob_list if \
        blob['name'].endswith('.parquet') and 'yesterday' in blob['name']]
    for parquet_blob in yesterday_parquet_list:
        container_client.get_blob_client(parquet_blob['name']).delete_blob()
    today_parquet_list = [blob for blob in blob_list if \
        blob['name'].endswith('.parquet') and 'today' in blob['name']]
    for parquet_blob in today_parquet_list:
        move_blob(
            container_client, 
            parquet_blob['name'], 
            parquet_blob['name'].replace('today', 'yesterday')
        )