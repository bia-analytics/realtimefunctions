from env_vars_lib import get_env_vars
from extract_load_lib import ExtractBlob


def main(filedata: dict) -> list:
    
    extract_blob = ExtractBlob(filedata['conn_string'])
    db_list = extract_blob.read_blob(
        'csv',
        filedata['container'],
        filedata['database_list_file_path'],
        filedata['database_list_file_name'],
        'dict'
    )

    return db_list