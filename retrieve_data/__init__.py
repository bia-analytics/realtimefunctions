from datetime import datetime
import logging

from azure.core.exceptions import ResourceExistsError
from azure.data.tables import TableServiceClient

from env_vars_lib import get_env_vars
from extract_load_lib import (
    ExtractBlob,
    extract_data,
    get_engine,
    get_tables_to_query,
    load_to_blob_storage,
    SalesTableComposer
)


def main(databaseconfig: dict) -> dict:
    
    env_vars = get_env_vars()

    host = databaseconfig['IP BASE']
    port = '1433'
    dbname = databaseconfig['DATABASE']
    user = databaseconfig['LOGIN BANCO']
    password = databaseconfig['SENHA BIA_ETL']
    timeout = env_vars.sql_timeout
    cod_loja = databaseconfig['CODIGO LOJA']
    desc_loja = databaseconfig['DESCRICAO LOJA']
    tables_to_query = get_tables_to_query()
    tables_to_query.set('STORE', 'store_code', f'{int(cod_loja):06}')

    engine = get_engine(
        host=host,
        port=port,
        dbname=dbname,
        user=user,
        password=password,
        timeout=timeout
    )  
    dfs = {}
    for table in tables_to_query.sections()[1:]:
        try:
            query_string = tables_to_query[table]['query']
            dfs[table.lower()] = extract_data(engine=engine, query_string=query_string)
        except Exception as e:
            return {
                'store': desc_loja,
                'status': f'could not query table data',
                'exception': f'{e}'
            }
    engine.dispose()

    extract_blob = ExtractBlob(env_vars.storage_conn_string)
    try:
        # df_vededores = extract_blob.try_recursive_read(
        #     int(env_vars.registry_tables_recursive_tries),
        #     'parquet',
        #     env_vars.source_container,
        #     env_vars.loja_vendedores_source_prefix_path,
        #     'loja_vendedores'
        # )
        df_filiais = extract_blob.try_recursive_read(
            int(env_vars.registry_tables_recursive_tries),
            'parquet',
            env_vars.source_container,
            env_vars.filiais_source_prefix_path,
            'filiais'
        )
        df_goals = extract_blob.read_blob(
            'csv',
            env_vars.source_container,
            env_vars.goals_blob_path,
            env_vars.goals_blob_name
        )
    except Exception as e:
        return {
            'store': desc_loja,
            'status': 'could not find batch table',
            'exception': f'{e}'
        }
    
    try:
        sales_df = SalesTableComposer().compose(
            sales_date=datetime.today().date(),
            df_loja_venda_pgto=dfs['loja_venda_pgto'],
            df_loja_venda=dfs['loja_venda'],
            df_filiais=df_filiais,
            df_goals=df_goals
        )
        if sales_df.empty:
            return {
                'store': desc_loja,
                'status': 'empty sales data',
                'exception': None
            }
    except Exception as e:
        return {
            'store': desc_loja,
            'status': 'could not compose sales table',
            'exception': f'{e}'
        }

    sales_df['PartitionKey'] = f'{int(cod_loja):06}'
    sales_df['RowKey'] = sales_df['FILIAL'] \
        + sales_df['TIPO_FILIAL'] \
        + sales_df['CANAL_FILIAL'] \
        + sales_df['REGIAO']
    sales_dict = sales_df.to_dict(orient='records')
    transactions = [('upsert', record) for record in sales_dict]
    table_service_client = TableServiceClient.from_connection_string(
        env_vars.storage_conn_string
    )
    try:
        table_client = table_service_client.create_table(
            env_vars.realtimesales_storage_table
        )
    except ResourceExistsError:
        table_client = table_service_client.get_table_client(
            env_vars.realtimesales_storage_table
        )
        
    try:
        table_client.submit_transaction(transactions)
    except Exception as e:
        return {
            'store': desc_loja,
            'status': 'could not upload data to table storage',
            'exception': f'{e}'
        }

    # for table_name, df in dfs.items():
    #     blob_path = 'linx_pos/RealTime/' \
    #         f'{int(cod_loja):06}_{desc_loja.replace(" ", "_")}' \
    #         f'/{table_name}/today/'
    #     today = datetime.today().date()
    #     blob_name = f'{table_name}_{today.strftime("%Y%m%d")}.parquet'
        
    #     try:
    #         load_to_blob_storage(
    #             dataframe=df,
    #             format='parquet',
    #             conn=env_vars.storage_conn_string,
    #             container=env_vars.source_container,
    #             blob_path=blob_path,
    #             blob_name=blob_name
    #         )
    #     except Exception as e:
    #         return {
    #             'store': desc_loja,
    #             'status': 'could not upload raw data to blob storage',
    #             'exception': f'{e}'
    #         }

    return {
        'store': desc_loja,
        'status': 'loaded successfully',
        'exception': None
    }